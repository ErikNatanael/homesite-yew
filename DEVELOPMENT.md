# Development

## Serve site

Trunk:

```
trunk serve --open
```

Tailwind:

```
npx tailwindcss -i ./src/css/main.css -o ./css/tailwind.css --watch
```

## Styling

### TailwindCSS

Using Tailwind for CSS seems really nice, since I suck at getting CSS right.

Tutorial for yew: https://issuecloser.com/blog/how-to-set-up-tailwind-css-with-yew-and-trunk

Custom local fonts in TailwindCSS: https://blog.logrocket.com/how-to-use-custom-fonts-tailwind-css/

#### Installation

```
npm i -g tailwindcss
```

#### Usage

To generate all the css:

```
tailwindcss -o ./tailwind.css
```

To run a minified production build:

```
NODE_ENV=production tailwindcss -c ./tailwind.config.js -o ./tailwind.css --minify
```

To rebuild at any change:

```
npx tailwindcss -i ./src/css/main.css -o ./css/tailwind.css --watch
```

### yewprint

Yew version of Blueprint.js, not complete but what's there looks nice.

https://yewprint.rm.rs/progress-bar
