use yew::prelude::*;
use yew_router::prelude::*;

use crate::Route;

#[derive(PartialEq, Copy, Clone, Eq)]
pub enum BioSection {
    Home,
    Musician,
    Composer,
    Coder,
    VisualArtist,
    InstrumentBuilder,
}
impl From<&str> for BioSection {
    fn from(name: &str) -> Self {
        match name {
            "musician" => Self::Musician,
            "composer" => Self::Composer,
            "coder" => Self::Coder,
            "visual_artist" => Self::VisualArtist,
            "instrument_builder" => Self::InstrumentBuilder,
            _ => Self::Home,
        }
    }
}
impl BioSection {
    fn text(&self) -> &'static str {
        match self {
            BioSection::Home => "Home text",
            BioSection::Musician => "Musician text!",
            BioSection::Composer => "Composer text!",
            BioSection::Coder => todo!(),
            BioSection::VisualArtist => todo!(),
            BioSection::InstrumentBuilder => todo!(),
        }
    }
}

#[derive(Properties, PartialEq)]
pub struct BioProps {
    #[prop_or("Home".to_string())]
    pub section: String,
}

#[function_component(Bio)]
pub fn bio(BioProps { section }: &BioProps) -> Html {
    let mut container_classes = classes!(
        "flex",
        "flex-row",
        "h-screen",
        "w-full",
        "margin-0",
        "overflow-hidden"
    );
    let mut sidebar_classes = classes!("basis-1/4", "bg-neutral-900", "border-2", "p-12");
    let mut main_area_classes = classes!("basis-3/4", "bg-neutral-900", "p-4", "border-2");
    let bio_text_classes = classes!("text-white", "text-l");
    let bottom_link_c = classes!("my-3");
    let section: BioSection = section.as_str().into();
    html! {
        <>
            <div class={container_classes}>
                <div class={sidebar_classes}>
                    <div>
            <h1 class={classes!("text-3xl")}>{"Erik Natanael Gustafsson"}</h1>
            <p class={bio_text_classes}>{"is a "}
                <BioLink text={"musician".to_owned()} route={Route::BioSpecific{section: "musician".to_string()}} />
                {", "}
                <BioLink text={"composer".to_owned()} route={Route::BioSpecific{section: "composer".to_string()}}/>
                {", "}
                <BioLink text={"coder".to_owned()} route={Route::BioSpecific{section: "coder".to_string()}}/>
                {", "}
                <BioLink text={"visual artist".to_owned()} route={Route::BioSpecific{section: "visual_artist".to_string()}}/>
                {" and "}
                <BioLink text={"instrument builder".to_owned()} route={Route::BioSpecific{section: "instrument_builder".to_string()}}/>
            </p>
            </div>
            <div>
                <div class={bottom_link_c.clone()}><BioLink text={"Commissions".to_owned()} route={Route::BioSpecific{section: "commussions".to_string()}}/></div>
                <div class={bottom_link_c.clone()}><BioLink text={"Contact".to_owned()} route={Route::BioSpecific{section: "contact".to_string()}}/></div>
                <div class={bottom_link_c.clone()}><BioLink text={"CV".to_owned()} route={Route::BioSpecific{section: "cv".to_string()}}/></div>
            </div>
            </div>
            <div class={main_area_classes}>
            <BioSectionDisplay {section} />
                </div>
            </div>
        </>
    }
}

#[derive(Properties, PartialEq)]
struct BioLinkProps {
    route: Route,
    text: String,
}
#[function_component(BioLink)]
fn bio_nav_link(BioLinkProps { text, route }: &BioLinkProps) -> Html {
    let label_c = classes!("text-white", "text-lg", "m-0");
    let link_c = classes!("underline");
    let img_c = classes!(
        "w-1/5",
        "min-w-[400px]",
        "aspect-[1.8/1]",
        "object-cover",
        "mt-3",
        "rounded-lg"
    );
    html! {
        <a class={link_c} href="#">
            <Link<Route> to={route.clone()}>
                <span class={label_c}>{text}</span>
            </Link<Route>>
            // <img class={img_c} src="https://eriknatanael.com/images/1/9/9/2/1/1992111f29bd916e81ad458ec401922fd9b623a0-screenshot2019-02-11-14-48-22-280.png"/>
        </a>
    }
}

#[derive(Properties, PartialEq)]
pub struct BioSectionProps {
    pub section: BioSection,
}
#[function_component(BioSectionDisplay)]
fn bio_section_display(BioSectionProps { section }: &BioSectionProps) -> Html {
    let container_c = classes!("bg-slate-900", "text-slate-100", "h-full");
    match section {
        BioSection::Home => html! {
             <BioMasonry />
        },
        _ => html! {
            <div class={container_c}>
                <p>
                {section.text()}
                </p>
            </div>
        },
    }
}

#[function_component(BioMasonry)]
fn bio_masonry() -> Html {
    let container_c = classes!("columns-3", "gap-8", "h-full");
    let img_c = classes!("w-full", "mb-8", "rounded");

    html! {

        <div class={container_c}>
             <img class={img_c.clone()} src="https://eriknatanael.com/images/8/4/b/c/5/84bc58b15e9928fedc8acb3dda90dbf4f355f7f9-11611161.jpg"/>
             <img class={img_c.clone()} src="https://eriknatanael.com/images/4/a/4/f/a/4a4fa1631d40af9c59e262cb5ebc1e0347653c69-11541154.jpg"/>
             <img class={img_c.clone()} src="https://eriknatanael.com/images/1/9/9/2/1/1992111f29bd916e81ad458ec401922fd9b623a0-screenshot2019-02-11-14-48-22-280.png"/>
             <img class={img_c.clone()} src="https://eriknatanael.com/images/1/9/9/2/1/1992111f29bd916e81ad458ec401922fd9b623a0-screenshot2019-02-11-14-48-22-280.png"/>
             <img class={img_c.clone()} src="https://eriknatanael.com/images/1/9/9/2/1/1992111f29bd916e81ad458ec401922fd9b623a0-screenshot2019-02-11-14-48-22-280.png"/>
             <img class={img_c.clone()} src="https://eriknatanael.com/images/1/9/9/2/1/1992111f29bd916e81ad458ec401922fd9b623a0-screenshot2019-02-11-14-48-22-280.png"/>
             <img class={img_c.clone()} src="https://eriknatanael.com/images/1/9/9/2/1/1992111f29bd916e81ad458ec401922fd9b623a0-screenshot2019-02-11-14-48-22-280.png"/>
             <img class={img_c.clone()} src="https://eriknatanael.com/images/1/9/9/2/1/1992111f29bd916e81ad458ec401922fd9b623a0-screenshot2019-02-11-14-48-22-280.png"/>
             <img class={img_c.clone()} src="https://eriknatanael.com/images/1/9/9/2/1/1992111f29bd916e81ad458ec401922fd9b623a0-screenshot2019-02-11-14-48-22-280.png"/>
            </div>
    }
}

// #[derive(Properties, PartialEq)]
// pub struct BioProps {
//     pub bio: BioData,
// }
// #[derive(PartialEq)]
// pub struct BioData {
//     pub portrait: String,
//     pub title: String,
//     pub short_bio: String,
//     pub media_iframe: String,
// }
// impl Default for BioData {
//     fn default() -> Self {
//         Self{
//         portrait: "Erik".into(),
//         title: "musician | artist".into(),
//         short_bio: "Erik Natanael Gustafsson is a musician, coder, composer and new media artist. His work brings the vulnerable and the invisible to the foreground through the use of sensitive sonic material and intricate software processes. We sit down with the tiny, the invisible, and the fragile to unveil a reverent and enchanted view of the world.".into(),
//         media_iframe: "<iframe style=\"border: 0; width: 400px; height: 340px;\" src=\"https://bandcamp.com/EmbeddedPlayer/album=493166283/size=large/bgcol=ffffff/linkcol=e99708/artwork=small/transparent=true/\" seamless=\"\"><a href=\"https://eriknatanael.bandcamp.com/album/neod-1-anortosit\" target=\"_blank\" rel=\"nofollow noopener noreferrer\" class=\"external-link no-image\">neod.1.anortosit by Erik Natanael</a></iframe>".into(),
//     }
//     }
// }
// #[function_component(ShortBio)]
// pub fn short_bio(props: &BioProps) -> Html {
//     html! {
//         <div class={classes!("shadow-lg", "bg-black","basis-full", "md:basis-1/4", "h-full", "p-6", "py-10")}>
//         <h1 class={classes!("text-3xl", "font-bold", "font-sans", "text-blue-200", "w-full", "pb-10")}>{"Erik Natanael"}</h1>
//             <img src={"https://eriknatanael.com/user/pages/05.about/02._bio-text/erik_rudan_neod_1080.jpg"}
//              height={"100"} class={classes!("flex-none", "float-left","hover:-rotate-6", "inline", "transition-all", "rounded-full", "h-36", "hover:blur-sm")} loading={"lazy"}/>
//             <p class={classes!("text-white")}>{&props.bio.short_bio}</p>
//             {&props.bio.media_iframe}
//             </div>

//     }
// }
