use std::rc::Rc;

use gloo_console::{console, console_dbg, log};
use gloo_timers::callback::Interval;
use rand::{thread_rng, Rng};
use yew::prelude::*;
use yew_router::prelude::*;
mod bio;
use bio::*;

#[derive(Clone, Routable, PartialEq)]
enum Route {
    #[at("/")]
    Home,
    #[at("/bio")]
    Bio,
    #[at("/bio/:section")]
    BioSpecific { section: String },
    #[at("/cloud")]
    Cloud,
    #[at("/music")]
    Music,
    #[at("/shop")]
    Shop,
    #[not_found]
    #[at("/404")]
    NotFound,
}

#[function_component(App)]
fn app() -> Html {
    html! {
        <>
            <HyperTutorial/>
        </>
    }
}

fn main() {
    yew::Renderer::<App>::new().render();
}

fn switch(routes: Route) -> Html {
    match routes {
        Route::Home => html! { <HyperMain /> },
        Route::Bio => html! { <Bio /> },
        Route::BioSpecific { section } => html! { <Bio {section} /> },
        Route::Cloud => html! {
            <ArticleCloudHOC />
        },
        Route::NotFound => html! { <h1>{ "404" }</h1> },
        Route::Music => html! {<h1>{"Music"}</h1>},
        Route::Shop => html! {<h1>{"Shop"}</h1>},
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum AppStateAction {
    ToggleNav,
    NavOff,
}
#[derive(Clone, Debug, PartialEq)]
pub struct AppState {
    nav_open: bool,
}
impl Reducible for AppState {
    type Action = AppStateAction;

    fn reduce(self: Rc<Self>, action: Self::Action) -> Rc<Self> {
        match action {
            AppStateAction::ToggleNav => AppState {
                nav_open: !self.nav_open,
            }
            .into(),
            AppStateAction::NavOff => AppState { nav_open: false }.into(),
        }
    }
}

pub type AppStateContext = UseReducerHandle<AppState>;

#[function_component(HyperTutorial)]
fn hyper_tutorial() -> Html {
    let ctx = use_reducer(|| AppState { nav_open: false });
    html! {
         <>
            <div class={classes!("bg-slate-900", "flex", "flex-col", "h-screen", "margin-0", "overflow-hidden", "text-white", "font-normal")}>
                <BrowserRouter>
                    <ContextProvider<AppStateContext> context={ctx}>
                        <Switch<Route> render={switch} />
                        <BottomNav />
                    </ContextProvider<AppStateContext>>
                </BrowserRouter>
            </div>
         </>
    }
}

#[derive(Properties, PartialEq)]
struct MovingCircleProps {
    circle_data: CircleData,
}

#[function_component(MovingCircle)]
fn moving_circle(MovingCircleProps { circle_data }: &MovingCircleProps) -> Html {
    let mut circle_classes = classes!(
        "w-60",
        "h-60",
        "absolute",
        "rounded-full",
        "transition",
        "duration-[3000ms]",
        "ease-in-out",
    );
    circle_classes.push("bg-slate-800");

    let mut image_div_classes = classes!(
        "transition",
        "duration-[100ms]",
        "ease-in-out",
        "bg-center",
        "bg-cover",
        "w-full",
        "h-full",
        "rounded-full",
    );
    // image_div_classes.push("bg-[url(https://eriknatanael.com/images/a/2/2/d/f/a22dfee6151b3c226c5dc8344b9c3b04734bc497-frame-12249.png)]");
    if circle_data.show_image {
        image_div_classes.push("opacity-100");
        image_div_classes.push("hover:opacity-50");
    } else {
        image_div_classes.push("opacity-0");
        image_div_classes.push("hover:opacity-50");
    }
    let style = format!(
        "transform: translate({}px, {}px) scale({});",
        circle_data.x, circle_data.y, circle_data.scale
    );
    let image_style = format!("background-image: url({});", circle_data.image_url);
    html! {
        <div class={circle_classes} style={style}>
            <div class={image_div_classes} style={image_style}></div>
        </div>
    }
}

#[derive(PartialEq, Clone)]
struct CircleData {
    x: i32,
    y: i32,
    scale: f32,
    show_image: bool,
    image_url: String,
}

enum Msg {
    Tick,
    FlickerImage,
}
#[function_component(ArticleCloudHOC)]
fn article_cloud_hoc() -> Html {
    let app_state_context = use_context::<AppStateContext>().expect("no ctx found");
    let app_state = &*app_state_context;
    html! {
        <ArticleCloud app_state={app_state.clone()} />
    }
}
#[derive(Properties, PartialEq)]
struct ArticleCloudProps {
    app_state: AppState,
}
struct ArticleCloud {
    circles: Vec<CircleData>,
    _interval: Interval,
    _flicker_interval: Interval,
}
impl Component for ArticleCloud {
    type Message = Msg;
    type Properties = ArticleCloudProps;

    fn create(ctx: &Context<Self>) -> Self {
        let callback = ctx.link().callback(|_| Msg::Tick);
        let interval = Interval::new(2000, move || callback.emit(()));
        let callback = ctx.link().callback(|_| Msg::FlickerImage);
        let flicker_interval = Interval::new(50, move || callback.emit(()));
        let mut rng = thread_rng();
        let images = vec![
            "https://eriknatanael.com/images/0/b/1/f/8/0b1f88ee01253c05d2e586a1da95cb5c7fbbebc4-frame-30134.png",
            "https://eriknatanael.com/images/a/2/2/d/f/a22dfee6151b3c226c5dc8344b9c3b04734bc497-frame-12249.png",
            "https://eriknatanael.com/images/3/c/e/a/3/3cea3ddf069ac313882f3b7239ee9d2143db56a0-screenshot20181211124805.jpg",
            "https://eriknatanael.com/images/a/a/d/d/9/aadd9e59bb04dd8ed00ce582d77685768b605c10-natureintheanthropocene1.jpg",
            "https://eriknatanael.com/user/pages/04.installations/_installations-hero/vlcsnap-2022-10-10-14h50m10s634.png",
            "https://eriknatanael.com/user/pages/04.installations/03.rfc-675-08/01._hero/rfc67508_bridge_header.jpg",
            "https://eriknatanael.com/images/f/7/1/a/0/f71a0d5078619bb9b618bdfebbe7b5c79a653918-img0160small.jpg",
            "https://eriknatanael.com/user/pages/04.installations/04.bcm/01._ci-poetry-hero/bcm-starry-sky_banner.jpeg",
            "https://eriknatanael.com/user/pages/04.installations/04.bcm/06._bcm-image-2/bcm-user.jpg",
        ];
        let mut circles = vec![];
        let window = gloo_utils::window();
        let width = window.inner_width().unwrap().as_f64().unwrap() as i32;
        let height = window.inner_height().unwrap().as_f64().unwrap() as i32;
        unsafe {
            log!("start middle");
            console_dbg!(width / 2, height / 2);
        }
        for image_url in images {
            circles.push(CircleData {
                x: rng.gen::<i32>().abs() % width,
                y: rng.gen::<i32>().abs() % height,
                scale: rng.gen::<f32>() * 0.8 + 0.2,
                show_image: false,
                image_url: image_url.to_string(),
            })
        }
        Self {
            circles,
            _interval: interval,
            _flicker_interval: flicker_interval,
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::Tick => {
                let mut rng = thread_rng();
                let window = gloo_utils::window();
                let width = window.inner_width().unwrap().as_f64().unwrap() as i32;
                let height = (window.inner_height().unwrap().as_f64().unwrap() * 0.8) as i32;
                for circle in &mut self.circles {
                    if rng.gen::<f32>() > 0.8 {
                        circle.x = rng.gen::<i32>().abs() % width;
                        circle.y = rng.gen::<i32>().abs() % height;
                        circle.scale = rng.gen::<f32>() * 0.8 + 0.2;
                    }
                }
                // the value has changed so we need to
                // re-render for it to appear on the page
                true
            }
            Msg::FlickerImage => {
                let mut rng = thread_rng();
                for circle in &mut self.circles {
                    if rng.gen::<f32>() > 0.99 {
                        circle.show_image = !circle.show_image;
                    }
                }
                // the value has changed so we need to
                // re-render for it to appear on the page
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let circles = self
            .circles
            .iter()
            .map(|circle| {
                html! {
                    <MovingCircle circle_data={circle.clone()} />
                }
            })
            .collect::<Html>();

        let mut main_classes = classes!("grow", "transition", "duration-500", "z-10", "bg-inherit");
        let app_state = &ctx.props().app_state;
        if app_state.nav_open {
            main_classes.push("-translate-y-1/2")
        }
        html! {
             <main class={main_classes}>
                {circles}
            </main>
        }
    }
}

#[function_component(HyperMain)]
fn hyper_main() -> Html {
    let selected_article = use_state(|| ArticleIndex(0));
    let nav_open = use_state(|| false);
    let articles = vec![
        Article {
            id: ArticleIndex(0),
            title: "How to build beauty".to_owned(),
            description: "We listen to the world and look at things".to_owned(),
        },
        Article {
            id: ArticleIndex(1),
            title: "Being me".to_owned(),
            description: "Rejoice and suffer".to_owned(),
        },
        Article {
            id: ArticleIndex(2),
            title: "Fragility".to_owned(),
            description: "I have water on my kwarm".to_owned(),
        },
    ];
    let on_click = {
        let selected_article = selected_article.clone();
        Callback::from(move |index: ArticleIndex| selected_article.set(index))
    };

    let mut main_classes = classes!("grow", "transition", "duration-500", "z-10");

    let app_state_context = use_context::<AppStateContext>().expect("no ctx found");
    let app_state = &*app_state_context;
    if app_state.nav_open {
        main_classes.push("-translate-y-1/2")
    }
    html! {
        <main class={main_classes}>
            <HyperArticle articles={articles} selected={*selected_article} on_click={on_click.clone()} />
        </main>
    }
}

#[derive(Clone, Copy, PartialEq)]
struct ArticleIndex(usize);
#[derive(Clone, PartialEq)]
struct Article {
    id: ArticleIndex,
    title: String,
    description: String,
}

#[derive(Properties, PartialEq)]
struct HyperArticleProps {
    articles: Vec<Article>,
    selected: ArticleIndex,
    on_click: Callback<ArticleIndex>,
}
#[function_component(HyperArticle)]
fn hyper_article(
    HyperArticleProps {
        articles,
        selected,
        on_click,
    }: &HyperArticleProps,
) -> Html {
    let article_class = classes!(
        "grid",
        "h-full",
        "grid-cols-[2fr_1fr]",
        "grid-rows-[2fr_1fr]"
    );
    let article_section = classes!("border", "border-white", "h-full", "flex");
    let mut article_image = article_section.clone();
    article_image.extend(vec![
        "bg-[url(https://eriknatanael.com/user/pages/01.home/01._hero/frame-31686.png)]",
        "bg-center",
        "bg-cover",
    ]);
    let mut article_title_section = article_section.clone();
    article_title_section.extend(vec![
        // "bg-gradient-to-r",
        // "from-cyan-500",
        // "to-blue-500",
        "align-center",
        "px-8",
        "py-16",
        "justify-between",
    ]);
    let article_title = classes![
        "basis-1/2",
        "text-6xl",
        "leading-[3rem]",
        "m-0",
        "uppercase"
    ];
    let mut article_nav_section = article_section.clone();
    article_nav_section.extend(vec![""]);
    let article_nav_button = classes!("grow", "bg-transparent", "text-6xl", "pointer");

    let article = &articles[selected.0];
    let on_back_callback = {
        let on_click = on_click.clone();
        let num_articles = articles.len();
        let currently_selected = selected.0;
        Callback::from(move |_| {
            let new_index = (currently_selected + num_articles - 1) % num_articles;
            on_click.emit(ArticleIndex(new_index));
        })
    };
    let on_forward_callback = {
        let on_click = on_click.clone();
        let num_articles = articles.len();
        let currently_selected = selected.0;
        Callback::from(move |_| {
            let new_index = (currently_selected + 1) % num_articles;
            on_click.emit(ArticleIndex(new_index));
        })
    };
    html! {
        <article class={article_class}>
            <div class={article_image}>
            </div>
            <div class={article_section.clone()}></div>
            <div class={article_title_section.clone()}>
                <h2 class={article_title.clone()}>{&article.id.0}</h2>
                <h2 class={article_title}>{&article.title}</h2>
            </div>
            <div class={article_section.clone()}>
                <button class={article_nav_button.clone()} type="button" onclick={on_back_callback}>
                    <i class="fa-solid fa-arrow-left-long"></i>
                </button>
                <button class={article_nav_button} type="button" onclick={on_forward_callback}>
                    <i class="fa-solid fa-arrow-right-long"></i>
                </button>
            </div>
        </article>
    }
}
#[function_component(BottomNavToggle)]
fn bottom_nav_toggle() -> Html {
    let app_state_context = use_context::<AppStateContext>().expect("no ctx found");
    let app_state = &*app_state_context.clone();
    let toggle_nav_callback = {
        Callback::from(move |_| {
            app_state_context.dispatch(AppStateAction::ToggleNav);
        })
    };
    let mut classes = classes!(vec![
        "h-20",
        "w-20",
        "fixed",
        "z-50",
        "left-1/2",
        "bottom-20",
        "rounded-full",
        "pointer",
        "outline-none",
        "border-none",
        // "transition-transform",
        // "transition-colors",
        "transition",
        "duration-300",
        "-translate-x-1/2",
        "hover:scale-105",
        "active:scale-95"
    ]);
    if app_state.nav_open {
        classes.push("bg-cyan-600");
    } else {
        classes.push("bg-amber-600");
    }
    html! {
        <div class={classes} onclick={toggle_nav_callback}></div>
    }
}

#[derive(Properties, PartialEq)]
struct NavLinkProps {
    route: Route,
    text: String,
}
#[function_component(BottomNavLink)]
fn bottom_nav_link(NavLinkProps { route, text }: &NavLinkProps) -> Html {
    let label_c = classes!("text-white", "text-lg", "m-0", "uppercase");
    let link_c = classes!("no-underline");
    let img_c = classes!(
        "w-1/5",
        "min-w-[400px]",
        "aspect-[1.8/1]",
        "object-cover",
        "mt-3",
        "rounded-lg"
    );
    let app_state_context = use_context::<AppStateContext>().expect("no ctx found");
    let nav_off_callback = {
        Callback::from(move |_| {
            app_state_context.dispatch(AppStateAction::NavOff);
        })
    };
    html! {
        <a class={link_c} href="#" onclick={nav_off_callback}>
            <Link<Route> to={route.clone()}>
                <h2 class={label_c}>{text}</h2>
            </Link<Route>>
            // <img class={img_c} src="https://eriknatanael.com/images/1/9/9/2/1/1992111f29bd916e81ad458ec401922fd9b623a0-screenshot2019-02-11-14-48-22-280.png"/>
        </a>
    }
}
#[function_component(BottomNav)]
fn bottom_nav() -> Html {
    let mut nav_classes = classes!("h-1/4", "w-full", "absolute", "left-0", "bottom-0");
    let mut nav_inner_c = classes!(
        "flex",
        "gap-4",
        "transition",
        "duration-500",
        "justify-center"
    );

    let app_state_context = use_context::<AppStateContext>().expect("no ctx found");
    let app_state = &*app_state_context.clone();
    if app_state.nav_open {
        nav_inner_c.push("translate-y-0");
        nav_inner_c.push("scale-100");
        nav_classes.push("z-30");
    } else {
        nav_inner_c.push("translate-y-full");
        nav_inner_c.push("scale-10");
        nav_classes.push("-z-50");
    }
    html! {
        <>
            <nav class={nav_classes}>
            <div class={nav_inner_c}>
            <BottomNavLink text={"Home".to_owned()} route={Route::Home} />
            <BottomNavLink text={"Bio".to_owned()} route={Route::Bio} />
            <BottomNavLink text={"Cloud".to_owned()} route={Route::Cloud} />
            <BottomNavLink text={"Music".to_owned()} route={Route::Music} />
            <BottomNavLink text={"Shop".to_owned()} route={Route::Shop} />
            </div>
            </nav>
            <BottomNavToggle />
        </>
    }
}
#[function_component(HyperNav)]
fn hyper_nav() -> Html {
    let nav_section_classes = classes!(
        "flex",
        "py-12",
        "px-8",
        "gap-4",
        "items-center",
        "justify-center",
        "box-border",
        "border-l"
    );
    let nav_toggle_button_classes = classes!(
        "align-center",
        "bg-transparent",
        "border-0",
        "text-white",
        "border-l",
        "gap-10",
        "px-0",
        "py-12",
        "z-50",
        "relative",
        "outline-0",
        "h-full",
        "hidden"
    );
    let mut nav_social_section_classes = nav_section_classes.clone();
    nav_social_section_classes.push("grow");
    let mut nav_link_section_classes = nav_section_classes.clone();
    nav_link_section_classes.push("gap-24");
    nav_link_section_classes.push("basis-1/2");
    html! {
        <nav class={classes!("flex", "w-full", "border-bottom", "border-white")}>
            <div id={"nav-logo-section"} class={classes!("basis-1/3", "justify-start")}>
            <Link<Route> to={Route::Home}>
                    <i class="fa-solid fa-dumpster-fire"></i>
            </Link<Route>>
            </div>
    <div id="nav-mobile-section" class={classes!("flex", "z-20", "basis-2/3")}>
        <div id="nav-link-section" class={nav_link_section_classes.clone()}>
            <a href="#">{"ABOUT"}</a>
            <Link<Route> to={Route::Cloud}>
                {"CLOUD"}
            </Link<Route>>
        </div>
        <div id="nav-social-section" class={nav_social_section_classes.clone()}>
          <a href="#">
            <i class="fa-brands fa-twitter"></i>
          </a>
          <a href="https://www.youtube.com/c/Hyperplexed" target="_blank">
            <i class="fa-brands fa-youtube"></i>
          </a>
          <a href="#">
            <i class="fa-brands fa-codepen"></i>
          </a>
        </div>
        <div id="nav-contact-section" class={nav_social_section_classes.clone()}>
            <a href="#">{"GET IN TOUCH"}</a>
        </div>
      </div>
      <button id="nav-toggle-button" class={nav_toggle_button_classes} type="button">
            <span class={classes!("inline-block", "h-4", "leading-4")}>{"Menu"}</span>
        <i class="fa-solid fa-bars"></i>
      </button>
                </nav>
            }
}

#[function_component(Splash)]
fn splash() -> Html {
    html! {
        <div class={classes!("bg-black", "basis-full", "md:basis-3/4", "p-8", "h-full", "text-white")}>
            <img src={"https://wallpaperset.com/w/full/a/a/0/102324.jpg"}  />
            <span class={classes!("font-bold")}>{"work title "}</span>
            <span class={classes!("font-italic")}>{"Picture from a work"}</span>
            </div>
    }
}

#[function_component(Menu)]
fn menu() -> Html {
    html! {
        <div class={classes!("flex", "flex-col", "md:flex-row", "px-10", "items-center")}>
            <div>{"thoughts"}</div>
            <div>{"projects"}</div>
            </div>
    }
}
